<?php

namespace App\Normalizer;

use App\Entity\User;
use App\Entity\Post;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserNormalizer implements ContextAwareNormalizerInterface
{
    /**
     * @param User $user
     * @param null $format
     * @param array $context
     * @return array|bool|float|int|mixed|string
     */
    public function normalize($user, $format = null, array $context = [])
    {
        if (!empty($user->getPosts())) {
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return $object instanceof User ? $object->getId() : $object;
                },
            ];

            $postNormalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        }
        //$data = $this->normalizer->normalize($user, $format, $context);

        return true;
    }


    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof User;
    }
}
