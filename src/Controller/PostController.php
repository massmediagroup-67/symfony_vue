<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Service\Serialize\SerializerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 */
class PostController extends AbstractController
{
    /** @var SerializerService */
    private $serializerService;

    public function __construct(SerializerService $serializerService)
    {
        $this->serializerService = $serializerService;
    }

    /**
     * @Route("/posts", name="posts.index")
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getManager()->getRepository(User::class)->find(3);//->findAllUsersWithIdNameThumbnail();
        //$posts = $this->getDoctrine()->getManager()->getRepository(Post::class)->find(2);
        dump($this->serializerService->serializeUserInJson($users));
        die();
        return $this->render('post/posts.html.twig', [
            'users' => $this->serializerService->serializeInJson($users),
            'posts' => $this->serializerService->serializeInJson($posts)
        ]);
    }
}
