<?php

namespace App\Admin\Post;

use App\Entity\Post;
use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use DateTime;

class PostAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name')
            ->add('user', ModelType::class, ['btn_add' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
            ->add('createAt')
            ->add('updateAt')
            ->add('user');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('createAt')
            ->add('updateAt')
            ->add('user');
    }

    public function prePersist($object)
    {
        /** @var Post $object */
        $object->setCreateAt(new DateTime());
        $object->setUpdateAt($object->getCreateAt());
    }

    public function preUpdate($object)
    {
        $object->setUpdateAt(new DateTime());
    }
}
