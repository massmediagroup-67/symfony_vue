<?php

namespace App\Admin\User;

use App\Entity\Post;
use App\Entity\User;
use App\Service\File\FileUploader;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UserAdmin extends AbstractAdmin
{
    const BASE_IMAGE_OPTIONS = [
        'required' => false,
        'data_class' => null
    ];

    /**
     * @var FileUploader
     */
    private $fileUploader;

    public function __construct($code, $class, $baseControllerName, FileUploader $fileUploader)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->fileUploader = $fileUploader;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name')
            ->add('image', FileType::class, $this->getImageOption(
                '/' . User::IMAGE_PATH . $this->getSubject()->getImageName()
            ))
            ->add('thumbnail', FileType::class, $this->getImageOption(
                '/' . User::THUMBNAIL_PATH . $this->getSubject()->getThumbnailName()
            ))
            ->add('posts', ModelType::class, ['required' => false, 'multiple' => true, 'btn_add' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
        ->add('posts');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('posts');
    }

    /**
     * @param string $publicPath
     * @return array
     */
    protected function getImageOption($publicPath)
    {
        if ($publicPath) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . $publicPath;

            return  array_merge(
                self::BASE_IMAGE_OPTIONS,
                ['help' => '<img src="' . $fullPath . '" class="admin-preview"/>']
            );
        }

        return self::BASE_IMAGE_OPTIONS;
    }

    /**
     * @param User $object
     */
    protected function setData(User $object)
    {
        if ($object->getImage()) {
            $object->setImageName($this->fileUploader->upload(
                $object->getImage(),
                User::IMAGE_PATH
            ));
        }

        if ($object->getThumbnail()) {
            $object->setThumbnailName($this->fileUploader->upload(
                $object->getThumbnail(),
                User::THUMBNAIL_PATH
            ));
        }

        if ($object->getPosts()) {
            /** @var Post $post */
            foreach ($object->getPosts() as $post) {
                $post->setUser($object);
            }
        }
    }

    public function prePersist($object)
    {
        $this->setData($object);
    }

    public function preUpdate($object)
    {
        $this->setData($object);
    }
}
