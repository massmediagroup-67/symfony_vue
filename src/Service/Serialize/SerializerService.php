<?php

namespace App\Service\Serialize;

use App\Entity\Post;
use App\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use App\Normalizer\UserNormalizer;

/**
 * Class EntitySerializerService
 * @package App\Service\Entity
 */
class SerializerService
{
    /**
     * @param array|object $data
     * @return string|null
     */
    public function serializeInJson($data)
    {
        if ($data) {
            return (new Serializer(
                [new ObjectNormalizer()],
                [new JsonEncoder()]
            ))->serialize($data, 'json');
        }

        return null;
    }

    /**
     * @param User $user
     * @return string|null
     */
    public function serializeUserInJson(User $user)
    {
        if ($user) {
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return $object instanceof User ? $object->getId() : $object;
                },
            ];

            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);


            return (new Serializer(
                [$normalizer],
                [new JsonEncoder()]
            ))->serialize($user, 'json');
        }

        return null;
    }
}
