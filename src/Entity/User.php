<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class User
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    const IMAGE_PATH = 'images/user/';
    const THUMBNAIL_PATH = 'images/user/thumbnail/';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @Assert\Image(
     *     maxSize = "2M"
     * )
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageName;

    /**
     * @Assert\Image(
     *     minWidth = 100,
     *     maxWidth = 200,
     *     minHeight = 100,
     *     maxHeight = 200
     * )
     */
    private $thumbnail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thumbnailName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="user")
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return User
     */
    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     * @return User
     */
    public function setThumbnail($thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     * @return User
     */
    public function setImageName($imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getThumbnailName()
    {
        return $this->thumbnailName;
    }

    /**
     * @param string $thumbnailName
     * @return User
     */
    public function setThumbnailName($thumbnailName): self
    {
        $this->thumbnailName = $thumbnailName;

        return $this;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     * @return User
     */
    public function setPosts(ArrayCollection $posts): self
    {
        $this->posts->clear();
        foreach ($posts as $post) {
            $this->addPost($post);
        }

        return $this;
    }

    /**
     * @param Post $post
     * @return User
     */
    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts->add($post);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName() ?? 'User';
    }
}
