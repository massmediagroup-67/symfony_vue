<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\Post;
use Doctrine\Common\Persistence\ManagerRegistry;

class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @return array
     */
    public function findAllInArray(): array
    {
        $query = $this->createQueryBuilder('p');

        return $query->select('p.id', 'p.name', 'p.user', 'p.createAt', 'p.updateAt')
            ->getQuery()
            ->getArrayResult();
    }
}
