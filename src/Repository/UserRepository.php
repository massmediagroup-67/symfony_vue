<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return array
     */
    public function findAllUsersWithIdNameThumbnail(): array
    {
        $query = $this->createQueryBuilder('u');

        return $query->select('u.id', 'u.name', 'u.thumbnailName')
            ->getQuery()
            ->getArrayResult();
    }
}
