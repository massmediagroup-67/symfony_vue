import Posts from '../vue/Posts'
import Vue from "vue"

Vue.config.devtools = true;
Vue.config.productionTip = false;

/* posts list */
new Vue({
    el: '#posts',
    // data: ['posts', 'users'],
    // template: '<Posts posts="posts" />',
    components: { Posts }

});

